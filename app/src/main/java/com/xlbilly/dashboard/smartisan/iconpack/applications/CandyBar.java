package com.xlbilly.dashboard.smartisan.iconpack.applications;

import android.support.annotation.NonNull;

import com.dm.material.dashboard.candybar.applications.CandyBarApplication;
import com.dm.material.dashboard.candybar.utils.JsonStructure;

public class CandyBar extends CandyBarApplication {
    
    @NonNull
    @Override
    public Configuration onInit() {
        //Sample configuration
        Configuration configuration = new Configuration();

        configuration.setGenerateAppFilter(true);
        configuration.setGenerateAppMap(true);
        configuration.setGenerateThemeResources(true);
        configuration.setWallpaperJsonStructure(
                new JsonStructure.Builder("wallpapers")
                        .name("name")
                        .author("author")
                        .url("url")
                        /* If thumbUrl set to null, url will be used as thumbUrl */
                        .thumbUrl(null)
                        .build());
        
        return configuration;
    }
}
